
/**
	@file main.c
	@brief Combines the HCS12 assembly interpreter functions with user input
	@author Natesh Narain
*/

/**
	@mainpage Retro-Computer

	@section intro Introduction

	The Retro Computer (System Simulator) was a project specfied for the Project course in Integrated Telecommunications and Computer Technology. 

	The goal of the project is the simulate the HCS12 microprocessor in a C program.

	The simulator in required to do as follows:

		- 8 & 16 bit arithmetic
			- add
			- subtract
			- multiply
			- divide

		- 8 bit logical operation
			- AND
			- OR
			- XOR

		- 8 & 16 bit storing to and loading from memory

		- implement all addressing modes

		- implement a hardware interface for the user:
			- input the assembler instruction as hexadecimal bytes
			- view the status of the programming model registers
			- allow for the execution of indiviual step or the whole program at once

	@section build Build Instructions

	Desktop

		- GCC
			- Ensure that gcc and make utilities are on your system path.
			- Enter Sources directory located in the root of project directory
			- Type "make all" into command window and hit ENTER
			- Binaries will be placed in bin directory

		- Cmake
			- Start cmake-gui application
			- Enter the source directory as Sources located at the root of the project directory
			- Enter the build directory
			- Choose a compile (Visual Studio, GCC, etc)
			- Click Configure and Generate

	Axman 

		- CodeWarrior
			- Open RetroComputer.mcp locate at root of project directory using the CodeWarrior IDE
			- Select target and click run

	@section hardware Hardware Description

	@image html retrocomputer-console-description.png "RetroComputer hardware interface"
	@image latex retrocomputer-console-description-2.png "RetroComputer hardware interface"

	The RetroComputer console allows the user to enter their HCS12 assembler program.

	8 Bit Input
		- Series of toggle switches used to enter the assembler instructions
	
	Hexadecimal Input
		- The RetroComputer Console uses a pair of programmable logic devices (GAL16V8B) to decode the 8 bits into a hexadecimal display on a corresponding pair of 7 segment displays

	RetroComputer Output
		- 8 LEDs used to display the programmer model registers (A, B, X, Y, PC, SP, CCR). 

	Operation Controls
		- Control the loading and execution of the assembler instructions. See @ref usage for more details
			-# Load\\Execute toggle for switching modes
			-# load\\step for entering and running instructions
			-# execute all the instructions (or remaining)

	@section usage Usage Instructions

	Console Usage

	-# Set Load\\Execute toggle switch to the left position, Load mode.
	-# Enter the desired byte using the toggle switches. A switch is HIGH when pointing towards the display and LOW when pointer away.
	 For example, enter : 10000110 on the switches will display the hex number 86 corresponding to LDAA. Refer to the instruction set summary for the HCS12 for all instructions
	-# Press the load button (#2 in operation controls)
	-# repeat 2 and 3 until the assembler program is loaded
	-# Set Load\\Execute toggle to right, Execute mode.
	-# Now you can choose whether to run the whole program or step through one instruction at a time.
	-# Use input switches to display the memory information. The following hexadecimal number will display the specified values
		- $00 -> CCR
		- $02 -> A
		- $04 -> B
		- $08 -> XH (Most significant byte)
		- $09 -> XL (Least significant byte)
		- $10 -> YH (Most significant byte)
		- $11 -> YL (Least significant byte)
		- $20 -> PCH (Most significant byte)
		- $21 -> PCL (Least siginicant byte)
		-  >= $22 -> Displays values in RAM. (i.e. Display $800 with $22, Display $801 with $23, etc)
*/

#include "AssemblyInterpreter.h"

#ifdef AXIOM
#include "ConsoleIO.h"
#endif // AXIOM

#ifdef VIRTUAL

#include <stdio.h>
#include <stdlib.h>

void printRegisters(struct Registers* reg);

#endif // VIRTUAL

// Define registers and memory as global structures to avoid HCS12 memory issues
struct Registers reg;
struct Memory mem;


#ifdef VIRTUAL
int main()
#endif
#ifdef AXIOM
void main()
#endif
{
	//
	int exitStatus;
	int i;

#ifdef AXIOM
	struct ConsoleState state;
#endif
	
	// Clear Registers
	reg.PC = 0;
	reg.AllPurpose.D = 0;
	reg.SP = 0;
	reg.X = 0;
	reg.Y = 0;
	reg.CCR = 0;
	
	i = 0;


#ifdef VIRTUAL

	mem.rom[i++] = LDAA_IMM;
	mem.rom[i++] = 0xFF;
	mem.rom[i++] = STAA_EXT;
	mem.rom[i++] = 0x08;
	mem.rom[i++] = 0x00;
	
	// execute the program and get its return code.
	exitStatus = execute(&reg, &mem, i);
	
	printf("STATUS: %s\n\n", (exitStatus) ? "Fail" : "OK");
	printRegisters(&reg);
	
	return exitStatus;
#endif

#ifdef AXIOM
    
    initDigitalIO();
    
	while(1){
	
		updateInputState(&state);

		// determine selected mode
		if(state.optPort.indiv.modeSelect == MODE_LOAD){
			// user is entering machine code

			if(state.optPort.indiv.stepOrLoad == PB_DOWN){
				// step or load button was pressed

				mem.rom[i++] = state.selectionInput;             // add machine code to ROM

				while(state.optPort.indiv.stepOrLoad == PB_DOWN)
				    updateInputState(&state); 					 // wait for button release before continuing

			}

		}
		else{
			// user is executing code

			if(state.optPort.indiv.exeAll == PB_DOWN){
				// user runs all assembler instruction at once

				while(state.optPort.indiv.exeAll == PB_DOWN)
				    updateInputState(&state); 					// wait for release before running instructions

				exitStatus = execute(&reg, &mem, i);

			}
			else if(state.optPort.indiv.stepOrLoad == PB_DOWN){
				// user chose to run only the next instruction

				while(state.optPort.indiv.stepOrLoad == PB_DOWN)
				    updateInputState(&state); 					// wait for release before running instructions

				exitStatus = step(&reg, &mem);

			}

			state.optPort.indiv.err = (exitStatus == EXIT_SUCCESS) ? 1 : 0; // write to error indicator

			updateOutputState(&state, &reg, &mem);                          // display information to user

		}


	} // while

	EnableInterrupts;
	for(;;) {
		_FEED_COP(); /* feeds the dog */
	}
#endif // AXIOM
}

#ifdef VIRTUAL

void printRegisters(struct Registers* reg)
{
	int i;

	printf("Note: Values are in Hexadecimal\n\n");
	printf("A:  %x\n", reg->AllPurpose.AB.A);
	printf("B:  %x\n", reg->AllPurpose.AB.B);
	printf("D:  %x\n", reg->AllPurpose.D);
	printf("X:  %x\n", reg->X);
	printf("Y:  %x\n", reg->Y);
	printf("PC: %x\n", reg->PC + ROM_START);
	printf("SP: %x\n", reg->SP);
	
	printf("\nCCR: SXHINZVC\n     ");
	
	for(i = 7; i >= 0; i--)
		printf("%d", isSet(reg->CCR, (1<<i) ) ? 1 : 0);
	printf("\n");
	
}

#endif

