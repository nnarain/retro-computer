
/**
	@file HCS12Assembler.h
	@brief Defines the HCS12 Assembler Commands
	@author Natesh Narain
*/

#ifndef HCS12ASSEMBLER
#define HCS12ASSEMBLER

#define DIR       0    ///< Direct Addressing Mode
#define EXT       1    ///< Extended Addressing Mode
#define IMM       2    ///< Immediate Addressing Mode
#define INH       3    ///< Inherent Addressing Mode
#define IDX       4    ///< Indexed Addressing Mode
#define IDX1      5    ///< Indexed Addressing Mode
#define IDX2      6    ///< Indexed Addressing Mode

#define REDIRECT  0x18 ///< Redirect to the second page of instructions

#define ABA_INH   0x06 ///< Adds Accumulator A to B -> A

#define ADDD_IMM  0xC3 ///<
#define ADDD_EXT  0xD3 ///<

#define ANDA_IMM  0x84 ///< Logical AND Accumulator A with Memory - Immediate
#define ANDA_DIR  0x94 ///< Logical AND Accumulator A with Memory - Direct
#define ANDA_EXT  0xB4 ///< Logical AND Accumulator A with Memory - Extended

#define ANDB_IMM  0xC4 ///< Logical AND Accumulator B with Memory - Immediate
#define ANDB_DIR  0xD4 ///< Logical AND Accumulator B with Memory - Direct
#define ANDB_EXT  0xF4 ///< Logical AND Accumulator B with Memory - Extended

#define BRA_REL   0x20 ///< Branch always - Relative addressing

#define EDIV_INH  0x11 ///< 
#define EMUL_INH  0x13 ///< 

#define EORA_IMM  0x88 ///< 
#define EORA_EXT  0xB8 ///< 

#define EORB_IMM  0xC8 ///< 
#define EORB_EXT  0xF8 ///< 

#define IDIV_INH  0x10 ///< 

#define INC_EXT   0x72  ///< Increment memory
#define INCA_INH  0x42  ///<
#define INCB_INH  0x52  ///<
#define INX_INH   0x08  ///<
#define INY_INH   0x02  ///<

#define JMP_EXT   0x06 ///< Subroutine Address -> PC - Extended
#define JMP_IDX   0x05 ///< Subroutine Address -> PC - Indexed

#define LDAA_IMM  0x86 ///< Load Accumulator A - Immediate
#define LDAA_DIR  0x96 ///< Load Accumulator A - Direct
#define LDAA_EXT  0xB6 ///< Load Accumulator A - Extended
#define LDAA_IDX  0xA6 ///< Load Accumulator A - Indexed

#define LDAB_IMM  0xC6 ///< Load Accumulator B - Immediate
#define LDAB_DIR  0xD6 ///< Load Accumulator B - Direct
#define LDAB_EXT  0xF6 ///< Load Accumulator B - Extended
#define LDAB_IDX  0xE6 ///< Load Accumulator B - Indexed

#define LDD_IMM   0xCC ///< 
#define LDD_EXT   0xFC ///< 

#define LDX_IMM   0xCE ///< 
#define LDX_EXT   0xFE ///< 

#define LDY_IMM   0xCD ///< 
#define LDY_EXT   0xFD ///< 

#define MUL_INH   0x12 ///< Multiple A and B -> A:B

#define ORAA_IMM  0x8A ///< Logical OR B with memory - Immediate
#define ORAA_DIR  0x9A ///< Logical OR B with memory - Direct
#define ORAA_EXT  0xBA ///< Logical OR B with memory - Extended

#define ORAB_IMM  0xCA ///< Logical OR B with memory - Immediate
#define ORAB_DIR  0xDA ///< Logical OR B with memory - Direct
#define ORAB_EXT  0xFA ///< Logical OR B with memory - Extended

#define SBA_INH   0x16 ///< Subtract B from A store in A

#define SUBD_IMM  0x83 ///< Subtract memory from D (A:B) - Immediate
#define SUBD_EXT  0xB3 ///< Subtract memory from D (A:B) - Extended

#define STAA_DIR  0x5A ///< Store Accumulator A to Memory - Direct
#define STAA_EXT  0x7A ///< Store Accumulator A to Memory - Extended
#define STAA_IDX  0x6A ///< Store Accumulator A to Memory - Indexed

#define STAB_DIR  0x5B ///< Store Accumulator A to Memory - Direct
#define STAB_EXT  0x7B ///< Store Accumulator A to Memory - Extended
#define STAB_IDX  0x6B ///< Store Accumulator A to Memory - Indexed

#define STD_DIR   0x5C ///< Store Double Accumulator D to Memory - Direct
#define STD_EXT   0x7C ///< Store Double Accumulator D to Memory - Extended
#define STD_IDX   0x6C ///< Store Double Accumulator D to Memory - Indexed

#endif