/**
	ConsoleIO.h
	@brief Defines the Input and Output ports for the RetroComputer Console
	@author Natesh Narain
*/

#ifndef CONSOLEIO
#define CONSOLEIO

#include <hidef.h>
#include "derivative.h"

#include "MemoryModel.h"
#include "opts.h" 

#define CLK 16000000UL

#define INPUT_PORT         PORTB ///< Input for machine from user
#define REGISTER_PORT1     PORTA ///< Output port for 8-bit register
#define OPERATION_PORT     PTT   ///< Register with the Retro Computer control

#define DDR_INPUT_PORT     DDRB  ///< Data Direction Register for Input Port
#define DDR_REGISTER_PORT1 DDRA  ///< Data Direction Register for Register output port
#define DDR_OPERATION_PORT DDRT  ///< Data Direction Register for Register operations port

#define MODE_EXECUTE       1     ///<
#define MODE_LOAD          0     ///<

#define PB_DOWN            0     ///< Pushbutton down state
#define PB_UP              1     ///< Pushbutton up state

#define DISPLAY_CCR        0x00	 ///< Input this hex byte on the Retro-Computer console to display the CCR
#define DISPLAY_A          0x02	 ///< Input this hex byte on the Retro-Computer console to display the A
#define DISPLAY_B          0x03	 ///< Input this hex byte on the Retro-Computer console to display the B
#define DISPLAY_XH         0x08	 ///< Input this hex byte on the Retro-Computer console to display the high byte of X
#define DISPLAY_XL         0x09	 ///< Input this hex byte on the Retro-Computer console to display the low byte of X
#define DISPLAY_YH         0x10	 ///< Input this hex byte on the Retro-Computer console to display the high byte of Y
#define DISPLAY_YL         0x11	 ///< Input this hex byte on the Retro-Computer console to display the low byte of Y
#define DISPLAY_PCH        0x20	 ///< Input this hex byte on the Retro-Computer console to display the high byte of PC
#define DISPLAY_PCL        0x21	 ///< Input this hex byte on the Retro-Computer console to display the low byte of PC
#define DISPLAY_WATCH      0x22  ///< Start point to display variables in RAM


//! The Data structure representing the current input an output state of the Retro Computer Console.
struct ConsoleState{

	unsigned int registerDisplay: 8;  ///< Display the register information
	
	unsigned int selectionInput: 8;   ///< User input machine, or which register to display
	
	//! Input and outputs used for controlling the Retro Computer
	union{
		struct{
			unsigned int err: 1;        ///< Error output LED
			unsigned int modeSelect: 1; ///< Select between Load and execute mode
			unsigned int exeAll: 1;     ///< Execute the entire assembler program
			unsigned int stepOrLoad: 1; ///< Run the next instruction
			unsigned int msnib: 4;      ///< The most significant nibble. (Not used)
		}indiv;                         ///< Access individual bits
		unsigned int all: 8;            ///< access to all information
	}optPort;
	
};

void delayMillis(long milli);
void initDigitalIO(void);
void updateInputState(struct ConsoleState* state);
void updateOutputState(struct ConsoleState* state, struct Registers* reg, struct Memory* mem);

#endif