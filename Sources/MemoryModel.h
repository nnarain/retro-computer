
/**
	@file MemoryModel.h
	@brief Defines memory components of HCS12
	@author Natesh Narain
*/

#ifndef MODEL
#define MODEL

/** \defgroup Memory Memory model 
* @{
*/

#define RAM_START 0x0800 ///< Location in memory where RAM starts
#define ROM_START 0x4000 ///< Location in memory where ROM starts

#define BLOCKS_RAM 0xDD ///<
#define BLOCKS_ROM 50   ///<

#define CCR_C 0b00000001 ///< Bit mask for the C bit in the CCR
#define CCR_V 0b00000010 ///< Bit mask for the V bit in the CCR
#define CCR_Z 0b00000100 ///< Bit mask for the Z bit in the CCR
#define CCR_N 0b00001000 ///< Bit mask for the N bit in the CCR
#define CCR_I 0b00010000 ///< Bit mask for the I bit in the CCR
#define CCR_H 0b00100000 ///< Bit mask for the H bit in the CCR
#define CCR_X 0b01000000 ///< Bit mask for the X bit in the CCR
#define CCR_S 0b10000000 ///< Bit mask for the S bit in the CCR


//! defines the simulations memory space
struct Memory{
	int ram[BLOCKS_RAM]; ///< Random Access Memory
	int rom[BLOCKS_ROM]; ///< Read Only Memory
};

//! Define all the registers in the programmer's model
struct Registers{

	//! All purpose registers. A,B = D
	union{
		struct{
#ifdef AXIOM
			unsigned int A: 8; ///< Accumulator B
			unsigned int B: 8; ///< Accumulator A
#endif
#ifdef VIRTUAL
            unsigned int B: 8; ///< Accumulator B
            unsigned int A: 8; ///< Accumulator A
#endif
		}AB;
		unsigned int D;        ///< Double Accumulator D
	}AllPurpose;

	unsigned int X;            ///< Index register
	unsigned int Y;            ///< Index register
	
	unsigned int PC;           ///< Program counter
	unsigned int SP;           ///< Stack pointer

	unsigned char CCR;	
};
/**@}*/

#endif





















