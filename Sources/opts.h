
/**
	@file opts.h
	@brief Defines simple bit operation macros
	@author Natesh Narain
*/

#ifndef OPTS
#define OPTS

//! Boolean type
typedef enum{
	true, false
}bool;

#define NIB  4  ///< Bits in a nibble
#define BYTE 8  ///< Bits in a byte
#define WORD 16 ///< Bits in a word

// Bit Masks
#define MASK_BIT0 0b00000001 ///< Bit Mask for bit 0
#define MASK_BIT1 0b00000010 ///< Bit Mask for bit 1
#define MASK_BIT2 0b00000100 ///< Bit Mask for bit 2
#define MASK_BIT3 0b00001000 ///< Bit Mask for bit 3
#define MASK_BIT4 0b00010000 ///< Bit Mask for bit 4
#define MASK_BIT5 0b00100000 ///< Bit Mask for bit 5
#define MASK_BIT6 0b01000000 ///< Bit Mask for bit 6
#define MASK_BIT7 0b10000000 ///< Bit Mask for bit 7

#define MASK_SIGNED_8BIT  0x0080     ///< Mask for the sign bit of a 8 number 
#define MASK_SIGNED_16BIT 0x8000     ///< Mask for the sign bit of a 16 number
#define MASK_SIGNED_32BIT 0x80000000 ///< Mask for the sign bit of a 32 number

/** \defgroup Bitwise Bitwise Macros
* @{
*/

#define set(x, y) x |= y	             	   ///< Sets x using bit mask y
#define clear(x, y) x &= ~y              	   ///< Clear x using bit mask y
#define bv(x) 1<<x			             	   ///< Gets the value of a bit
#define bitset(x,y,z) x = (z) ? x | y : x & ~y ///< change individual bit. x number to change, y -> mask, z -> value

#define isSet(x,y) x & y                 	   ///< Sets x given mask y
#define isClear(x,y) !(x & y)                  ///< Clears x given y

#define bitcat(x, y, bits) (x<<bits) + y 	   ///< Concatenate 2 bit patterns (i.e. 00001111, 00001111 = 111100001111)
#define nibcat(x, y) bitcat(x,y,NIB)     	   ///< Concatenate 2 nibbles
#define bytecat(x, y) bitcat(x,y,BYTE)   	   ///< Concatenate 2 bytes (i.e. 0x08, 0x01 = 0x0801)
#define wordcat(x, y) bitcat(x,y,WORD)   	   ///< Concatenate 2 words

/** @} */

#endif