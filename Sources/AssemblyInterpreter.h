/**
	AssemblyInterpreter.h
	@brief Contains core logic for interpreting the HCS12 Assembler instructions
	@author Natesh Narain
*/

#ifndef ASSEMBLY_INTERPRETER
#define ASSEMBLY_INTERPRETER

#include "MemoryModel.h"
#include "HCS12Assembler.h"
#include "opts.h"

#define EXIT_SUCCESS 0
#define EXIT_FAIL    1

int execute(struct Registers* reg, struct Memory* mem, int num);
int step(struct Registers* reg, struct Memory* mem);

void updateCCRForRegisters(struct Registers* reg, int flags, int signedMask);
void updateCCRFor8BitAddAndSubtract(struct Registers* reg, int flags, bool isSubtraction);
void updateCCRFor16BitAddAndSubtract(struct Registers* reg, int result, int M, bool isSubtraction);

int nextWord(unsigned int*, struct Memory*);

int redirect(struct Registers* reg, struct Memory* mem);

int ABA(struct Registers* reg, int mode);

int ADDD(struct Registers* reg, struct Memory* mem, int mode);

int ANDA(struct Registers* reg, struct Memory* mem, int mode);
int ANDB(struct Registers* reg, struct Memory* mem, int mode);

int BRA(struct Registers* reg, struct Memory* mem);

int EDIV(struct Registers* reg, struct Memory* mem, int mode);
int EMUL(struct Registers* reg, struct Memory* mem, int mode);

int EORA(struct Registers* reg, struct Memory* mem, int mode);
int EORB(struct Registers* reg, struct Memory* mem, int mode);

int IDIV(struct Registers* reg, struct Memory* mem, int mode);

int INC(struct Registers* reg, struct Memory* mem, int mode);
int INCA(struct Registers* reg, int mode);
int INCB(struct Registers* reg, int mode);
int INX(struct Registers* reg, int mode);
int INY(struct Registers* reg, int mode);

int JMP(struct Registers* reg, struct Memory* mem, int mode);

int LDAA(struct Registers* reg, struct Memory* mem, int mode);
int LDAB(struct Registers* reg, struct Memory* mem, int mode);
int LDD(struct Registers* reg, struct Memory* mem, int mode);
int LDX(struct Registers* reg, struct Memory* mem, int mode);
int LDY(struct Registers* reg, struct Memory* mem, int mode);

int MUL(struct Registers* reg, struct Memory* mem, int mode);

int ORAA(struct Registers* reg, struct Memory* mem, int mode);
int ORAB(struct Registers* reg, struct Memory* mem, int mode);

int SBA(struct Registers* reg, int mode);

int SUBD(struct Registers* reg, struct Memory* mem, int mode);

int STAA(struct Registers* reg, struct Memory* mem, int mode);
int STAB(struct Registers* reg, struct Memory* mem, int mode);

int STD(struct Registers* reg, struct Memory* mem, int mode);


#endif
