/**
	@file ConsoleIO.c
	@brief Defines the Input and Output ports for the RetroComputer Console
	@author Natesh Narain
*/

#include "ConsoleIO.h"

void delayMillis(int milli)
{
    int i;
    for(i = 0; i < milli * CLK; i++);
}

/**
	@brief Initialize Digital IO input and output
*/
void initDigitalIO(void)
{
	DDR_INPUT_PORT = 0x00;
	DDR_REGISTER_PORT1 = 0xFF;
	DDR_OPERATION_PORT = 0x01;
	
	REGISTER_PORT1 = 0x00;
}

/**
	@brief Update the input state of the console

	@param state
		state to update

*/
void updateInputState(struct ConsoleState* state)
{
	state->selectionInput = INPUT_PORT;
	state->optPort.all = OPERATION_PORT;
}

/**
	@brief Update the output LEDs on the RetroComputer Console

	@param state
		the state to output

	@param reg
		programming model registers

	@param mem
		RAM and ROM

*/
void updateOutputState(struct ConsoleState* state, struct Registers* reg, struct Memory* mem)
{
	// display the information that the user has specified on the input switches
	switch(state->selectionInput){

		case DISPLAY_CCR:
			REGISTER_PORT1 = reg->CCR;
			break;

		case DISPLAY_A:
			REGISTER_PORT1 = reg->AllPurpose.AB.A;
			break;

		case DISPLAY_B:
			REGISTER_PORT1 = reg->AllPurpose.AB.B;
			break;

		case DISPLAY_XH:
			REGISTER_PORT1 = reg->X >> BYTE;
			break;

		case DISPLAY_XL:
			REGISTER_PORT1 = reg->X & 0x00FF;
			break;

		case DISPLAY_YH:
			REGISTER_PORT1 = reg->Y >> BYTE;
			break;

		case DISPLAY_YL:
			REGISTER_PORT1 = reg->Y & 0x00FF;
			break;

		case DISPLAY_PCH:
			REGISTER_PORT1 = (reg->PC+ROM_START) >> BYTE;
			break;

		case DISPLAY_PCL:
			REGISTER_PORT1 = (reg->PC+ROM_START) & 0x00FF;
			break;
			
		default:
		    if(state->selectionInput >= DISPLAY_WATCH){
		        REGISTER_PORT1 = mem->ram[state->selectionInput-DISPLAY_WATCH];
		    }
		    break;

	}
}