#
# RetroComputer Desktop Sources
#
# Author: Natesh Narain
#
# Created: March 28, 2014
# Last Updated: March 28, 2014

CMAKE_MINIMUM_REQUIRED(VERSION 2.8 FATAL_ERROR)

SET(appName RetroComputer)
PROJECT(${appName})

ADD_DEFINITIONS(-DVIRTUAL)

ADD_EXECUTABLE(
	${appName} 
	main.c 
	AssemblyInterpreter.c
	AssemblyInterpreter.h
	MemoryModel.h
	HCS12Assembler.h
	opts.h
)