/**
	@file AssemblyInterpreter.c
	@brief Contain functions to execute HCS12 Assembler instructions
	@author Natesh Narain
*/
	
#ifdef VIRTUAL
#include <stdio.h>
#endif

#include "AssemblyInterpreter.h"

/**
	@brief Execute the specified number of instructions stored in ROM
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param num
		The number of bytes to iterate over in ROM
	
*/
int execute(struct Registers* reg, struct Memory* mem, int num)
{
	int status;

	while(reg->PC < num){
		
		status = step(reg, mem);
		
		if(status == EXIT_FAIL)
			return status;
	}
	return status;
}

/**
	@brief runs the current instruction specified by the program counter
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
	
*/
int step(struct Registers* reg, struct Memory* mem)
{
	int status;

	// match the current instruction with the command
	switch(mem->rom[reg->PC])
	{
		case REDIRECT:
			status = redirect(reg, mem);
			break;
		
		case ADDD_IMM:
			status = ADDD(reg, mem, IMM);
			break;

		case ADDD_EXT:
			status = ADDD(reg, mem, EXT);
			break;

		case ANDA_IMM:
			status = ANDA(reg, mem, IMM);
			break;
	
		case ANDA_EXT:
			status = ANDA(reg, mem, EXT);
			break;
			
		case ANDB_IMM:
			status = ANDB(reg, mem, IMM);
			break;
			
		case ANDB_EXT:
			status = ANDB(reg, mem, EXT);
			break;
	
		case BRA_REL:
			status = BRA(reg, mem);
			break;

		case EDIV_INH:
			status = EDIV(reg, mem, INH);
			break;

		case EMUL_INH:
			status = EMUL(reg, mem, INH);
			break;
	
		case EORA_IMM:
			status = EORA(reg, mem, IMM);
			break;
			
		case EORA_EXT:
			status = EORA(reg, mem, EXT);
			break;
			
		case EORB_IMM:
			status = EORB(reg, mem, IMM);
			break;
			
		case EORB_EXT:
			status = EORB(reg, mem, EXT);
			break;
	
		case LDAA_IMM:
			status = LDAA(reg, mem, IMM);
			break;
			
		case LDAA_DIR:
			status = LDAA(reg, mem, DIR);
			break;
			
		case LDAA_EXT:
			status = LDAA(reg, mem, EXT);
			break;
		
		case LDAB_IMM:
			status = LDAB(reg, mem, IMM);
			break;
			
		case LDD_IMM:
			status = LDD(reg, mem, IMM);
			break;
			
		case LDD_EXT:
			status = LDD(reg, mem, EXT);
			break;
			
		case LDX_IMM:
			status = LDX(reg, mem, IMM);
			break;
			
		case LDX_EXT:
			status = LDX(reg, mem, EXT);
			break;
			
		case LDY_IMM:
			status = LDY(reg, mem, IMM);
			break;
			
		case LDY_EXT:
			status = LDY(reg, mem, EXT);
			break;
			
		case MUL_INH:
			status = MUL(reg, mem, INH);
			break;
			
		case ORAA_EXT:
			status = ORAA(reg, mem, EXT);
			break;
			
		case ORAB_EXT:
			status = ORAB(reg, mem, EXT);
			break;
			
		case STAA_DIR:
			status = STAA(reg, mem, DIR);
			break;
			
		case STAA_EXT:
			status = STAA(reg, mem, EXT);
			break;
		
		case STAB_EXT:
			status = STAB(reg, mem, EXT);
			break;
			
		default:
			status = EXIT_FAIL;
			break;
	}
	
	reg->PC++; // move to next instruction
	
#ifdef VIRTUAL
	printf("status: %d\n", status);
#endif
	return status;
}

/**
	Gets the next word from ROM and increments the Program Counter accordingly

	@param PC
		The pointer to the program counter

	@param mem
		The locations blocks for accessing RAM and ROM

*/
int nextWord(unsigned int* PC, struct Memory* mem)
{
	int addr = bytecat(mem->rom[*PC+1], mem->rom[*PC+2]);
	*PC += 2;
	return addr;
}

/**
	@brief Redirects program counter to next page of instructions
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
	
*/
int redirect(struct Registers* reg, struct Memory* mem)
{
	int status;

	switch(mem->rom[++reg->PC]){
	
		case ABA_INH:
			status = ABA(reg, INH);
			break;
	
		case IDIV_INH:
			status = IDIV(reg, mem, INH);
			break;
	
		case SBA_INH:
			status = SBA(reg, INH);
			break;
	
		default:
			return EXIT_FAIL;
	
	}
	
	return status;
}

/**
	@brief Update the Condition Code Register
	
	@param reg
		The programming model registers to run commands against
		
	@param flags
		This variable used to determine how to change the CCR
	
*/
void updateCCRForRegisters(struct Registers* reg, int flags, int signedMask)
{
	bitset(reg->CCR, CCR_N, (isSet(flags, signedMask)) ? 1 : 0);
	bitset(reg->CCR, CCR_Z, (flags) ? 0 : 1);
	clear(reg->CCR, CCR_V);
}

/**
	@brief Update the CCR for 8 bit arithmetic operations

	@param reg
		The programming model registers to run commands against
		
	@param flags
		This variable used to determine how to change the CCR
	
*/
void updateCCRFor8BitAddAndSubtract(struct Registers* reg, int flags, bool isSubtraction)
{
	unsigned char A7 = reg->AllPurpose.AB.A >> (BYTE-1);
	unsigned char B7 = reg->AllPurpose.AB.B >> (BYTE-1);
	unsigned char R7 = flags >> (BYTE-1);

	bitset(reg->CCR, CCR_N, (isSet(flags, MASK_SIGNED_8BIT)) ? 1 : 0);
	bitset(reg->CCR, CCR_Z, (reg->AllPurpose.AB.A) ? 0 : 1);
	bitset(reg->CCR, CCR_V, (A7 & B7 & ~R7) | (~A7 & ~B7 & R7));

	if(isSubtraction == true){
		bitset(reg->CCR, CCR_C, (~A7 & B7) | (B7 & R7) | (R7 & ~A7));
	}
	else{
		bitset(reg->CCR, CCR_C, (A7 & B7) | (B7 & ~R7) | (~R7 & A7));
	}

}

/**
	@brief Update CCR for 16 bit arithmetic

	@param reg
		programming model registers

	@param result
		result of operation

	@param M
		memory used to add or subtract

	@param isSubtraction
		what type of operation it is

*/
void updateCCRFor16BitAddAndSubtract(struct Registers* reg, int result, int M, bool isSubtraction)
{
	unsigned char D15 = reg->AllPurpose.D >> 15;
	unsigned char M15 = M >> 15;
	unsigned char R15 = result >> 15;

	bitset(reg->CCR, CCR_N, (isSet(result, bv(15))) ? 1 : 0 );
	bitset(reg->CCR, CCR_Z, (result) ? 0 : 1 );

	if(isSubtraction == true){
		bitset(reg->CCR, CCR_C, (~D15 & M15) | (M15 & R15) | (R15 & ~D15));
	}
	else{
		bitset(reg->CCR, CCR_C, (D15 & M15) | (M15 & ~R15 & D15));
	}

}

/**
	@brief Adds B to A and stores in A
	
	@param reg
		The programming model registers to run commands against
		
	@param mode
		Addressing Mode
*/
int ABA(struct Registers* reg, int mode)
{
	int flags;
	unsigned char A3 = reg->AllPurpose.AB.A >> 3;
	unsigned char B3 = reg->AllPurpose.AB.B >> 3;
	unsigned char R3;


	if(mode == INH){

		flags = reg->AllPurpose.AB.A + reg->AllPurpose.AB.B;

		updateCCRFor8BitAddAndSubtract(reg, flags, false);
		
		R3 = (reg->AllPurpose.AB.A & 0x04) >> 3;
		bitset(reg->CCR, CCR_H, (A3 & B3) | (B3 & ~R3) | (~R3 & A3));

		reg->AllPurpose.AB.A = flags;

	}
	else{
		return EXIT_FAIL;
	}

	return EXIT_SUCCESS;
}

/**
	@brief Adds memory to double accumulator D. (A:B) + M -> (A:B)
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int ADDD(struct Registers* reg, struct Memory* mem, int mode)
{
	int val;
	int result;

	if(mode == IMM){
	
		val = nextWord(&reg->PC, mem);
		
		result = reg->AllPurpose.D + val;
	
	}
	else if(mode == EXT){
	
		int address;

		address = nextWord(&reg->PC, mem) - RAM_START;
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
		
		val = bytecat(mem->ram[address], mem->ram[address+1]);
		result = reg->AllPurpose.D + val;
	
	}
	else{
		return EXIT_FAIL;
	}
	
	updateCCRFor16BitAddAndSubtract(reg, result, val, false);
	reg->AllPurpose.D = result;

	return EXIT_SUCCESS;
}

/**
	@brief Logical AND A with Memory
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int ANDA(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == IMM){
	
		reg->AllPurpose.AB.A &= mem->rom[++reg->PC];
	
	}
	else if(mode == EXT){
	
		// get address from next to bytes
		int address = nextWord(&reg->PC, mem) - RAM_START;
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
			
		reg->AllPurpose.AB.A &= mem->ram[address];
	}
	
	// update CCR
	updateCCRForRegisters(reg, reg->AllPurpose.AB.A, MASK_SIGNED_8BIT);
	
	return EXIT_SUCCESS;
}

/**
	@brief Logical AND A with Memory
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int ANDB(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == IMM){
	
		reg->AllPurpose.AB.B &= mem->rom[++reg->PC];
	
	}
	else if(mode == EXT){
	
		// get address from next to bytes
		int address = nextWord(&reg->PC, mem) - RAM_START;
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
			
		reg->AllPurpose.AB.B &= mem->ram[address];
	}
	
	// update CCR
	updateCCRForRegisters(reg, reg->AllPurpose.AB.B, MASK_SIGNED_8BIT);
	
	return EXIT_SUCCESS;
}

/**
	Branch Always

	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM

*/
int BRA(struct Registers* reg, struct Memory* mem)
{
	signed char offset = (signed char) mem->rom[++reg->PC];
	reg->PC += offset;
	return EXIT_SUCCESS;
}

/**
	@brief (Y:D) / (X) -> Y; R -> D
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
	
*/
int EDIV(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == INH){
		if(reg->X != 0){
			long temp = wordcat(reg->Y, reg->AllPurpose.D);
			reg->AllPurpose.D = temp % reg->X;
			reg->Y = temp / reg->X;
		
			bitset(reg->CCR, CCR_N, (isSet(temp, MASK_SIGNED_16BIT)));
			bitset(reg->CCR, CCR_Z, (temp) ? 0 : 1 );
			bitset(reg->CCR, CCR_V, (temp > 0xFFFF) ? 1 : 0 );
		}else{
			bitset(reg->CCR, CCR_C, 1);
		}
	}
	else{
		return EXIT_FAIL;
	}
	
	return EXIT_SUCCESS;
}

/**
	@brief Multiple D and Y -> (Y:D)
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
	
*/
int EMUL(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == INH){
	
		long temp = reg->AllPurpose.D * reg->Y;
		reg->Y = (temp & 0xFFFF0000)>>WORD;    // Eliminate the least significant word and shift into a 16-bit size
		reg->AllPurpose.D = temp & 0x0000FFFF; // Eliminate the most significant word

		bitset(reg->CCR, CCR_N, isSet(temp, MASK_SIGNED_32BIT));
		bitset(reg->CCR, CCR_Z, (temp) ? 0 : 1);
		bitset(reg->CCR, CCR_C, isSet(temp, MASK_SIGNED_16BIT) ? 1 : 0);
	
	}
	else{
		return EXIT_FAIL;
	}
	
	return EXIT_SUCCESS;
}

/**
	@brief Logical XOR A with Memory
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int EORA(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == IMM){
	
		reg->AllPurpose.AB.A ^= mem->rom[++reg->PC];
	
	}else if(mode == EXT){
	
		int address = nextWord(&reg->PC, mem) - RAM_START;
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
			
		reg->AllPurpose.AB.A ^= mem->ram[address];
	
	}
	
	updateCCRForRegisters(reg, reg->AllPurpose.AB.A, MASK_SIGNED_8BIT);

	return EXIT_SUCCESS;
}

/**
	@brief Logical XOR B with Memory
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int EORB(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == IMM){
	
		reg->AllPurpose.AB.B ^= mem->rom[++reg->PC];
	
	}else if(mode == EXT){
	
		int address = nextWord(&reg->PC, mem) - RAM_START;
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
			
		reg->AllPurpose.AB.B ^= mem->ram[address];
	}

	updateCCRForRegisters(reg, reg->AllPurpose.AB.B, MASK_SIGNED_8BIT);
	
	return EXIT_SUCCESS;
}

/**
	@brief Divide D by X store in X, remainder in D
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
	
*/
int IDIV(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == INH){
		if(reg->X != 0){

			int r = reg->AllPurpose.D % reg->X;
			reg->X = reg->AllPurpose.D / reg->X;
			
			reg->AllPurpose.D = r;

			//reg->CCR.indiv.V = 0;
			clear(reg->CCR, CCR_V);
			//reg->CCR.indiv.Z = (reg->X == 0) ? 1 : 0;
			bitset(reg->CCR, CCR_Z, (reg->X == 0) ? 1 : 0);

		}
		else{
			bitset(reg->CCR, CCR_C, 1);
			reg->X = 0xFFFF;
		}
	}
	else{
		return EXIT_FAIL;
	}
	
	return EXIT_SUCCESS;
}

/**
	@brief Increment memory

	@param reg
		programming model registers

	@param mem
		RAM and ROM

	@param mode
		addressing mode
*/
int INC(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == EXT){

	}
	else if(mode == IDX){

	}
	else{
		return EXIT_FAIL;
	}

	return EXIT_SUCCESS;
}

/**
	@brief Increment A

	@param reg
		programming model registers

	@param mode
		addressing mode
*/
int INCA(struct Registers* reg, int mode)
{
	if(mode ==  INH){

		reg->AllPurpose.AB.A += 0x01;

	}
	else{
		return EXIT_FAIL;
	}

	return EXIT_SUCCESS;
}

/**
	@brief Increment B

	@param reg
		programming model registers

	@param mode
		addressing mode
*/
int INCB(struct Registers* reg, int mode)
{
	if(mode ==  INH){

		reg->AllPurpose.AB.B += 0x01;

	}
	else{
		return EXIT_FAIL;
	}

	return EXIT_SUCCESS;
}

/**
	@brief Increment X

	@param reg
		programming model registers

	@param mode
		addressing mode
*/
int INX(struct Registers* reg, int mode)
{
	if(mode ==  INH){

		reg->X += 0x01;

	}
	else{
		return EXIT_FAIL;
	}

	return EXIT_SUCCESS;
}

/**
	@brief Increment Y

	@param reg
		programming model registers

	@param mode
		addressing mode
*/
int INY(struct Registers* reg, int mode)
{
	if(mode ==  INH){

		reg->Y += 0x01;

	}
	else{
		return EXIT_FAIL;
	}

	return EXIT_SUCCESS;
}

/**
	@brief Set the Program Counter to a Subroutine Address
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
	
*/
int JMP(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == EXT){
	
		int address = nextWord(&reg->PC, mem) - ROM_START;
		
		if(address > BLOCKS_ROM)
			return EXIT_FAIL;
			
		reg->PC = mem->rom[address];
	
	}else if(mode == IDX){
	
	}else{
		return EXIT_FAIL;
	}
	
	return EXIT_SUCCESS;
}

/**
	@brief Load Accumulator A
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int LDAA(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == IMM){
		
		reg->AllPurpose.AB.A = mem->rom[++reg->PC];

	}
	else if(mode == EXT){
	
		// get address using next 2 bytes
		int address = nextWord(&reg->PC, mem) - RAM_START;
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
		
		mem->ram[address] = reg->AllPurpose.AB.A;
	
	}
	
	updateCCRForRegisters(reg, reg->AllPurpose.AB.A, MASK_SIGNED_8BIT);
	
	return EXIT_SUCCESS;
	
}

/**
	@brief Load Accumulator B
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int LDAB(struct Registers* reg, struct Memory* mem, int mode)
{
	
	if(mode == IMM){
		
		reg->AllPurpose.AB.B = mem->rom[++reg->PC];
		
	}
	else if(mode == DIR){
	
		int address = mem->rom[++reg->PC] - RAM_START; // 
		
		// check if address is out of bounds
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
			
		mem->ram[address] = reg->AllPurpose.AB.B;	

	}
	else if(mode == EXT){

		// get address using next 2 bytes
		int address = nextWord(&reg->PC, mem) - RAM_START;
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
			
		mem->ram[address] = reg->AllPurpose.AB.B;
	
	}
	
	updateCCRForRegisters(reg, reg->AllPurpose.AB.A, MASK_SIGNED_8BIT);
	
	return EXIT_SUCCESS;
	
}

/**
	@brief Load Accumulator D
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int LDD(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == IMM){
	
		reg->AllPurpose.D = nextWord(&reg->PC, mem);
	
	}
	else if(mode == EXT){
	
		int address = nextWord(&reg->PC, mem) - RAM_START;
		reg->AllPurpose.D = bytecat(mem->ram[address], mem->ram[address+1]);
	}
	else{
		return EXIT_FAIL;
	}

	updateCCRForRegisters(reg, reg->AllPurpose.D, MASK_SIGNED_16BIT);

	return EXIT_SUCCESS;
}

/**
	@brief Load X
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int LDX(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == IMM){
	
		reg->X = nextWord(&reg->PC, mem);
	
	}
	else if(mode == EXT){
	
		int address = nextWord(&reg->PC, mem) - RAM_START;
		reg->X = bytecat(mem->ram[address], mem->ram[address+1]);
	}
	else{
		return EXIT_FAIL;
	}

	updateCCRForRegisters(reg, reg->X, MASK_SIGNED_16BIT);
	
	return EXIT_SUCCESS;
}

/**
	@brief Load Y
	
	@param reg
	The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int LDY(struct Registers* reg, struct Memory* mem, int mode)
{

	if(mode == IMM){

		reg->Y = nextWord(&reg->PC, mem);
	
	}
	else if(mode == EXT){
	
		int address = nextWord(&reg->PC, mem) - RAM_START;
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
		
		reg->Y = bytecat(mem->ram[address], mem->ram[address+1]);
	}
	else{
		return EXIT_FAIL;
	}
	
	updateCCRForRegisters(reg, reg->Y, MASK_SIGNED_16BIT);

	return EXIT_SUCCESS;
}

/**
	Multiple A with B store in D(A:B)
	
	@param reg
	The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int MUL(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == INH){
	
		reg->AllPurpose.D = reg->AllPurpose.AB.A * reg->AllPurpose.AB.B;
	
	}
	else{
		return EXIT_FAIL;
	}
	
	bitset(reg->CCR, CCR_C, (isSet(reg->AllPurpose.AB.B, MASK_SIGNED_8BIT)) ? 1 : 0); 

	return EXIT_SUCCESS;
}

/**
	@brief Logical OR A with Memory
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int ORAA(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == IMM){
		
		reg->AllPurpose.AB.A |= mem->rom[++reg->PC];
		
	}
	else if(mode == EXT){
	
		int address = nextWord(&reg->PC, mem) - RAM_START;
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
			
		reg->AllPurpose.AB.A |= mem->ram[address];
	
	}

	updateCCRForRegisters(reg, reg->AllPurpose.AB.A, MASK_SIGNED_8BIT);
	
	return EXIT_SUCCESS;
}

/**
	@brief Logical OR B with Memory
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int ORAB(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == IMM){
		
		reg->AllPurpose.AB.B |= mem->rom[++reg->PC];
		
	}
	else if(mode == EXT){
	
		int address = nextWord(&reg->PC, mem) - RAM_START;
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
			
		reg->AllPurpose.AB.B |= mem->ram[address];
	
	}
	
	updateCCRForRegisters(reg, reg->AllPurpose.AB.B, MASK_SIGNED_8BIT);

	return EXIT_SUCCESS;
}

/**
	@brief Subtracts B from A and stores in A
	
	@param reg
		The programming model registers to run commands against
		
	@param mode
		Addressing Mode
*/
int SBA(struct Registers* reg, int mode)
{
	int flags;
	unsigned char A7 = reg->AllPurpose.AB.A >> (BYTE-1);
	unsigned char B7 = reg->AllPurpose.AB.B >> (BYTE-1);
	unsigned char R7;

	if(mode == INH){
		flags = reg->AllPurpose.AB.A - reg->AllPurpose.AB.B;
		reg->AllPurpose.AB.A = flags;
	}
	else{
		return EXIT_FAIL;
	}
	
	updateCCRFor8BitAddAndSubtract(reg, flags, true);
	
	R7 = flags >> (BYTE-1);
	bitset(reg->CCR, CCR_C, (~A7 & B7) | (B7 & R7) | (R7 & ~A7));
	
	return EXIT_SUCCESS;
}

/**
	Subtracts memory from D (A:B)

	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode

*/
int SUBD(struct Registers* reg, struct Memory* mem, int mode)
{
	int val;

	if(mode == IMM){

		val = nextWord(&reg->PC, mem);

		reg->AllPurpose.D += val;

	}
	else if(mode == EXT){

		int address;

		address = nextWord(&reg->PC, mem) - RAM_START;

		if(address > BLOCKS_RAM)
			return EXIT_FAIL;

		val = bytecat(mem->ram[address], mem->ram[address+1]);

		reg->AllPurpose.D += val;

	}
	else{
		return EXIT_FAIL;
	}

	updateCCRFor16BitAddAndSubtract(reg, reg->AllPurpose.D, val, true);

	return EXIT_SUCCESS;
}

/**
	@brief Store Accumulator A in Memory
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int STAA(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == EXT){

		int address = nextWord(&reg->PC, mem) - RAM_START;
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
		
		mem->ram[address] = reg->AllPurpose.AB.A;
	
	}else if(mode == IDX){
	
		// get address of data using next byte as offset to address stored in X Register
		int address = reg->X + mem->rom[++reg->PC];
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
		
		mem->ram[address] = reg->AllPurpose.AB.A;
	
	}
	
	updateCCRForRegisters(reg, reg->AllPurpose.AB.A, MASK_SIGNED_8BIT);
	
	return EXIT_SUCCESS;
}

/**
	@brief Stores Accumulator B in memory
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int STAB(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == EXT){
	
		int address = bytecat(mem->rom[reg->PC+1], mem->rom[reg->PC+2]) - RAM_START;
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
		
		mem->ram[address] = reg->AllPurpose.AB.B;
		
		reg->PC += 2;
	
	}else if(mode == IDX){
	
		// get address of data using next byte as offset to address stored in X Register
		int address = reg->X + mem->rom[++reg->PC];
		
		if(address > BLOCKS_RAM)
			return EXIT_FAIL;
		
		mem->ram[address] = reg->AllPurpose.AB.B;
	
	}
	
	updateCCRForRegisters(reg, reg->AllPurpose.AB.B, MASK_SIGNED_8BIT);
	
	return EXIT_SUCCESS;
}

/**
	@brief Stores Accumulator B in memory
	
	@param reg
		The programming model registers to run commands against
		
	@param mem
		The locations blocks for accessing RAM and ROM
		
	@param mode
		Addressing Mode
*/
int STD(struct Registers* reg, struct Memory* mem, int mode)
{
	if(mode == EXT){

		int address = nextWord(&reg->PC, mem) - ROM_START;

		if(address > BLOCKS_RAM || address+1 > BLOCKS_RAM)
			return EXIT_FAIL;

		mem->ram[address] = reg->AllPurpose.D & 0x00FF;
		mem->ram[address+1] = reg->AllPurpose.D >> BYTE;

	}
	else if(mode == IDX){

	}
	else{
		return EXIT_FAIL;
	}

    updateCCRForRegisters(reg, reg->AllPurpose.D, MASK_SIGNED_16BIT);

	return EXIT_SUCCESS;
}






