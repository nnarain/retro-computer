\contentsline {chapter}{\numberline {1}Retro-\/\discretionary {-}{}{}Computer}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Build Instructions}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Hardware Description}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Usage Instructions}{3}{section.1.4}
\contentsline {chapter}{\numberline {2}Module Index}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Modules}{5}{section.2.1}
\contentsline {chapter}{\numberline {3}Data Structure Index}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Data Structures}{7}{section.3.1}
\contentsline {chapter}{\numberline {4}File Index}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}File List}{9}{section.4.1}
\contentsline {chapter}{\numberline {5}Module Documentation}{11}{chapter.5}
\contentsline {section}{\numberline {5.1}Memory model}{11}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Detailed Description}{11}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Bitwise Macros}{12}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Detailed Description}{12}{subsection.5.2.1}
\contentsline {chapter}{\numberline {6}Data Structure Documentation}{13}{chapter.6}
\contentsline {section}{\numberline {6.1}Console\discretionary {-}{}{}State Struct Reference}{13}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Detailed Description}{13}{subsection.6.1.1}
\contentsline {section}{\numberline {6.2}Memory Struct Reference}{13}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Detailed Description}{14}{subsection.6.2.1}
\contentsline {section}{\numberline {6.3}Registers Struct Reference}{14}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Detailed Description}{14}{subsection.6.3.1}
\contentsline {chapter}{\numberline {7}File Documentation}{15}{chapter.7}
\contentsline {section}{\numberline {7.1}Assembly\discretionary {-}{}{}Interpreter.\discretionary {-}{}{}c File Reference}{15}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Detailed Description}{17}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Function Documentation}{17}{subsection.7.1.2}
\contentsline {subsubsection}{\numberline {7.1.2.1}A\discretionary {-}{}{}B\discretionary {-}{}{}A}{17}{subsubsection.7.1.2.1}
\contentsline {subsubsection}{\numberline {7.1.2.2}A\discretionary {-}{}{}D\discretionary {-}{}{}D\discretionary {-}{}{}D}{18}{subsubsection.7.1.2.2}
\contentsline {subsubsection}{\numberline {7.1.2.3}A\discretionary {-}{}{}N\discretionary {-}{}{}D\discretionary {-}{}{}A}{19}{subsubsection.7.1.2.3}
\contentsline {subsubsection}{\numberline {7.1.2.4}A\discretionary {-}{}{}N\discretionary {-}{}{}D\discretionary {-}{}{}B}{20}{subsubsection.7.1.2.4}
\contentsline {subsubsection}{\numberline {7.1.2.5}B\discretionary {-}{}{}R\discretionary {-}{}{}A}{21}{subsubsection.7.1.2.5}
\contentsline {subsubsection}{\numberline {7.1.2.6}E\discretionary {-}{}{}D\discretionary {-}{}{}I\discretionary {-}{}{}V}{21}{subsubsection.7.1.2.6}
\contentsline {subsubsection}{\numberline {7.1.2.7}E\discretionary {-}{}{}M\discretionary {-}{}{}U\discretionary {-}{}{}L}{22}{subsubsection.7.1.2.7}
\contentsline {subsubsection}{\numberline {7.1.2.8}E\discretionary {-}{}{}O\discretionary {-}{}{}R\discretionary {-}{}{}A}{22}{subsubsection.7.1.2.8}
\contentsline {subsubsection}{\numberline {7.1.2.9}E\discretionary {-}{}{}O\discretionary {-}{}{}R\discretionary {-}{}{}B}{23}{subsubsection.7.1.2.9}
\contentsline {subsubsection}{\numberline {7.1.2.10}execute}{24}{subsubsection.7.1.2.10}
\contentsline {subsubsection}{\numberline {7.1.2.11}I\discretionary {-}{}{}D\discretionary {-}{}{}I\discretionary {-}{}{}V}{25}{subsubsection.7.1.2.11}
\contentsline {subsubsection}{\numberline {7.1.2.12}I\discretionary {-}{}{}N\discretionary {-}{}{}C}{26}{subsubsection.7.1.2.12}
\contentsline {subsubsection}{\numberline {7.1.2.13}I\discretionary {-}{}{}N\discretionary {-}{}{}C\discretionary {-}{}{}A}{26}{subsubsection.7.1.2.13}
\contentsline {subsubsection}{\numberline {7.1.2.14}I\discretionary {-}{}{}N\discretionary {-}{}{}C\discretionary {-}{}{}B}{27}{subsubsection.7.1.2.14}
\contentsline {subsubsection}{\numberline {7.1.2.15}I\discretionary {-}{}{}N\discretionary {-}{}{}X}{27}{subsubsection.7.1.2.15}
\contentsline {subsubsection}{\numberline {7.1.2.16}I\discretionary {-}{}{}N\discretionary {-}{}{}Y}{27}{subsubsection.7.1.2.16}
\contentsline {subsubsection}{\numberline {7.1.2.17}J\discretionary {-}{}{}M\discretionary {-}{}{}P}{28}{subsubsection.7.1.2.17}
\contentsline {subsubsection}{\numberline {7.1.2.18}L\discretionary {-}{}{}D\discretionary {-}{}{}A\discretionary {-}{}{}A}{28}{subsubsection.7.1.2.18}
\contentsline {subsubsection}{\numberline {7.1.2.19}L\discretionary {-}{}{}D\discretionary {-}{}{}A\discretionary {-}{}{}B}{29}{subsubsection.7.1.2.19}
\contentsline {subsubsection}{\numberline {7.1.2.20}L\discretionary {-}{}{}D\discretionary {-}{}{}D}{30}{subsubsection.7.1.2.20}
\contentsline {subsubsection}{\numberline {7.1.2.21}L\discretionary {-}{}{}D\discretionary {-}{}{}X}{31}{subsubsection.7.1.2.21}
\contentsline {subsubsection}{\numberline {7.1.2.22}L\discretionary {-}{}{}D\discretionary {-}{}{}Y}{32}{subsubsection.7.1.2.22}
\contentsline {subsubsection}{\numberline {7.1.2.23}M\discretionary {-}{}{}U\discretionary {-}{}{}L}{33}{subsubsection.7.1.2.23}
\contentsline {subsubsection}{\numberline {7.1.2.24}next\discretionary {-}{}{}Word}{34}{subsubsection.7.1.2.24}
\contentsline {subsubsection}{\numberline {7.1.2.25}O\discretionary {-}{}{}R\discretionary {-}{}{}A\discretionary {-}{}{}A}{34}{subsubsection.7.1.2.25}
\contentsline {subsubsection}{\numberline {7.1.2.26}O\discretionary {-}{}{}R\discretionary {-}{}{}A\discretionary {-}{}{}B}{35}{subsubsection.7.1.2.26}
\contentsline {subsubsection}{\numberline {7.1.2.27}redirect}{35}{subsubsection.7.1.2.27}
\contentsline {subsubsection}{\numberline {7.1.2.28}S\discretionary {-}{}{}B\discretionary {-}{}{}A}{36}{subsubsection.7.1.2.28}
\contentsline {subsubsection}{\numberline {7.1.2.29}S\discretionary {-}{}{}T\discretionary {-}{}{}A\discretionary {-}{}{}A}{37}{subsubsection.7.1.2.29}
\contentsline {subsubsection}{\numberline {7.1.2.30}S\discretionary {-}{}{}T\discretionary {-}{}{}A\discretionary {-}{}{}B}{38}{subsubsection.7.1.2.30}
\contentsline {subsubsection}{\numberline {7.1.2.31}S\discretionary {-}{}{}T\discretionary {-}{}{}D}{39}{subsubsection.7.1.2.31}
\contentsline {subsubsection}{\numberline {7.1.2.32}step}{40}{subsubsection.7.1.2.32}
\contentsline {subsubsection}{\numberline {7.1.2.33}S\discretionary {-}{}{}U\discretionary {-}{}{}B\discretionary {-}{}{}D}{43}{subsubsection.7.1.2.33}
\contentsline {subsubsection}{\numberline {7.1.2.34}update\discretionary {-}{}{}C\discretionary {-}{}{}C\discretionary {-}{}{}R\discretionary {-}{}{}For16\discretionary {-}{}{}Bit\discretionary {-}{}{}Add\discretionary {-}{}{}And\discretionary {-}{}{}Subtract}{44}{subsubsection.7.1.2.34}
\contentsline {subsubsection}{\numberline {7.1.2.35}update\discretionary {-}{}{}C\discretionary {-}{}{}C\discretionary {-}{}{}R\discretionary {-}{}{}For8\discretionary {-}{}{}Bit\discretionary {-}{}{}Add\discretionary {-}{}{}And\discretionary {-}{}{}Subtract}{45}{subsubsection.7.1.2.35}
\contentsline {subsubsection}{\numberline {7.1.2.36}update\discretionary {-}{}{}C\discretionary {-}{}{}C\discretionary {-}{}{}R\discretionary {-}{}{}For\discretionary {-}{}{}Registers}{45}{subsubsection.7.1.2.36}
\contentsline {section}{\numberline {7.2}Console\discretionary {-}{}{}I\discretionary {-}{}{}O.\discretionary {-}{}{}c File Reference}{45}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Detailed Description}{46}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Function Documentation}{46}{subsection.7.2.2}
\contentsline {subsubsection}{\numberline {7.2.2.1}update\discretionary {-}{}{}Input\discretionary {-}{}{}State}{46}{subsubsection.7.2.2.1}
\contentsline {subsubsection}{\numberline {7.2.2.2}update\discretionary {-}{}{}Output\discretionary {-}{}{}State}{47}{subsubsection.7.2.2.2}
\contentsline {section}{\numberline {7.3}H\discretionary {-}{}{}C\discretionary {-}{}{}S12\discretionary {-}{}{}Assembler.\discretionary {-}{}{}h File Reference}{48}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}Detailed Description}{51}{subsection.7.3.1}
\contentsline {section}{\numberline {7.4}main.\discretionary {-}{}{}c File Reference}{51}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}Detailed Description}{52}{subsection.7.4.1}
\contentsline {subsection}{\numberline {7.4.2}Variable Documentation}{52}{subsection.7.4.2}
\contentsline {subsubsection}{\numberline {7.4.2.1}i}{52}{subsubsection.7.4.2.1}
\contentsline {section}{\numberline {7.5}Memory\discretionary {-}{}{}Model.\discretionary {-}{}{}h File Reference}{53}{section.7.5}
\contentsline {subsection}{\numberline {7.5.1}Detailed Description}{54}{subsection.7.5.1}
\contentsline {section}{\numberline {7.6}opts.\discretionary {-}{}{}h File Reference}{54}{section.7.6}
\contentsline {subsection}{\numberline {7.6.1}Detailed Description}{55}{subsection.7.6.1}
\contentsline {chapter}{Index}{56}{section*.16}
